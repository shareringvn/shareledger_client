let { TypeFactory, Types } = require('js-amino');

let Coin = TypeFactory.create('Coin', [{
        name: "denom",
        type: Types.String
    },
    {
        name: "amount",
        type: Types.Int64
    }
])

create = coinObj => {
    let coin = new Coin(coinObj.denom, coinObj.amount)    
    return coin; 
}

module.exports = {
    create
}