let { TypeFactory, Types } = require('js-amino');
let PubKey = require("./publicKey")

let SignatureSecp256k1 = TypeFactory.create('SignatureSecp256k1', [{
        name: "bytes",
        type: Types.ByteSlice
    } 
], Types.ByteSlice)

let BasicSignature = TypeFactory.create('BasicSignature', [{
    name: "pubKey",
    type: Types.ByteSlice
},
{
    name: "signature",
    type: Types.ByteSlice
}  
])

let AuthSignature = TypeFactory.create('AuthSignature', [{
    name: "pubKey",
    type: Types.Interface
},
{
    name: "signature",
    type: Types.Interface
},
{
    name: "nonce",
    type: Types.Int64
}    
])

createBasicSignature = (bzKey, bzSig) => {
    let sig = new SignatureSecp256k1(bzSig) 
    let  pubKey = PubKey.createPubKeySecp256k1(bzKey)  
    return new BasicSignature(pubKey,sig); 
}

createAuthSignature = (bzKey,bzSig,nonce) => {
    let sig = new SignatureSecp256k1(bzSig) 
    let  pubKey = PubKey.createPubKeySecp256k1(bzKey)  
    return new AuthSignature(pubKey,sig,nonce); 
}

registerConcrete = codec => {
    codec.registerConcrete(new SignatureSecp256k1(), "shareledger/SigSecp256k1", {});
    codec.registerConcrete(new BasicSignature(), "shareledger/BasicSig", {})
    codec.registerConcrete(new AuthSignature(), "shareledger/AuthSig", {})
    PubKey.registerConcrete(codec)
	//codec.RegisterConcrete(AuthSig{}, "shareledger/AuthSig", nil)
}

module.exports = {
    createBasicSignature,
    registerConcrete,
    createAuthSignature
}