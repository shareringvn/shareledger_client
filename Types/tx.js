let {
    TypeFactory,
    Types
} = require('js-amino');

let Utils = require('../utils')
let Signature = require('./signature')
let MsgSend = require('./../Messages/bank/msgSend')
let MsgLoad = require('./../Messages/bank/msgLoad')
let MsgCheck = require('./../Messages/bank/msgCheck')
let MsgCreate = require('./../Messages/asset/msgCreate')
let MsgDelete = require('./../Messages/asset/msgDelete')
let MsgRetrieve = require('./../Messages/asset/msgRetrieve')
let MsgUpdate = require('./../Messages/asset/msgUpdate')
let MsgBook = require('./../Messages/booking/msgBook')
let MsgComplete = require('./../Messages/booking/msgComplete')
let MsgNonce = require('../Messages/auth/msgNonce')

let BasicTx = TypeFactory.create('BasicTx', [{
        name: "msg",
        type: Types.Interface
    },
    {
        name: "signature",
        type: Types.Struct
    }
])

let AuthTx = TypeFactory.create('AuthTx', [{
        name: "msg",
        type: Types.Interface
    },
    {
        name: "signature",
        type: Types.Struct
    }
])

let QueryTx = TypeFactory.create('QueryTx', [{
    name: "msg",
    type: Types.Interface
}
])

createBasicTx = (msg, sig) => {
    let basicTx = new BasicTx(msg, sig)
    return basicTx;
}

createAuthTx = (msg, sig) => {
    let authTx = new AuthTx(msg, sig)
    return authTx;
}

createQueryTx = msg => {
    let queryTx = new QueryTx(msg)
    return queryTx;
}

createSendTx = (sendingObj, keyPair) => {
    let sendTx = createTxWithNonce(sendingObj, keyPair, MsgSend)
    return sendTx
}

createCreditTx = (creditObj, keyPair) => {    
    let creditTx = createTxWithNonce(creditObj, keyPair, MsgLoad)
    return creditTx
}

createAssetCreationTx = (assetObj, keyPair) => {    
    let creationTx = createTxWithNonce(assetObj, keyPair, MsgCreate)
    return creationTx
}

createAssetUpdateTx = (assetObj,keyPair) => {
    let updateTx = createTxWithNonce(assetObj, keyPair, MsgUpdate)
    return updateTx
}

createAssetDeleteTx = (obj, keyPair) => {    
    let deleteTx = createTxWithNonce(obj, keyPair, MsgDelete)
    return deleteTx
}

createAssetRetrievalTx = uuid => {
    let retrieveMsg = MsgRetrieve.create(uuid)   
    let queryTx = new QueryTx(retrieveMsg)
    return queryTx
}

createCheckBalanceTx = account => {
    let queryMsg = MsgCheck.create(account)
    let checkTx = new QueryTx(queryMsg)
    return checkTx
}

createBookingTx = (bookObj,keyPair) => {
    let updateTx = createTxWithNonce(bookObj, keyPair, MsgBook)
    return updateTx
}

createBookingCompletionTx = (completionObj, keyPair ) => {
    let completionTx = createTxWithNonce(completionObj, keyPair, MsgComplete)
    return completionTx;
}

createNonceQueryTx = address => {
    let nonceMsg = MsgNonce.create(address)
    let nonceTx = new QueryTx(nonceMsg)
    return nonceTx
}

//utility function
createTxWithNonce = (obj, keyPair, Msg) => {
    let msg = Msg.create(obj)
    let bzSig = keyPair.sign(obj.nonce + Msg.getSignBytes(msg))    
    bzSig = Utils.hexToBytes(bzSig);
    let bzKey = Utils.hexToBytes(keyPair.pubKey)
    let sig = Signature.createAuthSignature(bzKey, bzSig, obj.nonce)
    let tx = createAuthTx(msg, sig)
    return tx
}


registerConcrete = codec => {
    codec.registerConcrete(new BasicTx(), "shareledger/BasicTx", {});
    codec.registerConcrete(new AuthTx(), "shareledger/AuthTx", {});
    codec.registerConcrete(new QueryTx(), "shareledger/QueryTx", {});
}

module.exports = {
    createBasicTx,
    registerConcrete,
    createAuthTx,
    createSendTx,
    createCreditTx,
    createAssetCreationTx,
    createAssetDeleteTx,
    createAssetRetrievalTx,
    createAssetUpdateTx,
    createBookingTx,
    createBookingCompletionTx,
    createNonceQueryTx,
    createCheckBalanceTx
}
