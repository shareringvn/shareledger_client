# Shareledger Client

Library to interact with ShareLedger



## Transactions
**NOTE**: Please pass the object with members in the correct order as described below. Failing to adhere to this rule, the transaction will generate an incorrect signature.

* The transaction is *based64*-encoded to prepare for transmission between platform


* Credit Transaction



* Check Balance



* Send transaction


### Asset Creation
* creator
* hash
* uuid
* status
* fee


### Asset Retrieval
* uuid

### Asset Update
* creator
* hash
* uuid
* status
* fee


### Asset Deletion
* uuid


### Book 
* nonce
* renter
* uuid
* duration

### Complete
* nonce
* renter
* bookingId


## Signed Transaction

For each type of transaction, an additional parameter `keyPair` is passed. If this parameter is non-null, it is used to sign the transaction. This *keyPair* is of type *KeyPair* defined in ShrCrypto (shr-keys) library


An example of signed transaction:
```json
{
  "type": "86BEFD870A4DD0",
  "value": {
    "message": {
      "type": "EF832CA69AF9B8",
      "value": {
        "nonce": 1,
        "from": "112233",
        "amount": {
          "denom": "SHR",
          "amount": 1
        }
      }
    },
    "signature": {
      "pub_key": {
        "type": "88B6D5D73C58D0",
        "value": "MDQwODkwZTIwYmU5OTExMjkzYTQ0NGQ5YTUzMGYwZjIwMjM0YWRiZjY0NjE0MjIyOWRiOGJiNjQ3MzJjODljZmQwZGZhNmYyOWJiZGU3MzNlYWNjZTI4ZGZlNWU0YWYyY2M1YWU0YmJmMzQ2ZjgzNTBkZmViYTBiNjYzNzYwNjZiMQ=="  // base64 encoded
      },
      "signature": {
        "type": "414D8BA932E620",
        "value": "MzA0NTAyMjEwMGNmZDlkN2E4ZDUzYzIyMDk4MWQ3OGIwNTNmZDRjZTY4YzBkZjUxYThjNmMzNDU2MGM0OGY1OGVmZDIxMDg5MDkwMjIwMGMxZjYyOTc3NTk2OTcyYjc3ZjkwMGEyMjVlNGM5OWFjZTYxM2E0N2QzYTQyZjRkYTc0NmRlYjhmMmY4YWE4YQ==" // base64 encoded
      }
    }
  }
}
```

