const arrToHex = (arr, hasPrefix = true) => {

    let hex = arr.map(e => {
        return ("00" + (e & 0xFF).toString(16)).slice(-2)
    }).join('')
    if (hasPrefix) return '0x'.concat(hex)
    return hex
}

const strToBytes = str => {
    var bytes = [];
    for (var i = 0, n = str.length; i < n; i++) {
        var char = str.charCodeAt(i);
        bytes.push(char >>> 8, char & 0xFF);
    }
    return bytes;
}

const asciiToHex = str => {
    var arr = [];
    for (let n = 0, l = str.length; n < l; n++) {
        var hex = Number(str.charCodeAt(n)).toString(16);
        arr.push(hex);
    }
    return arr.join('');
}


const hexToBytes = hex => {
    for (var bytes = [], c = 0; c < hex.length; c += 2)
        bytes.push(parseInt(hex.substr(c, 2), 16));
    return bytes;
}

const bytesToHex =  bytes => {
    for (var hex = [], i = 0; i < bytes.length; i++) {
        hex.push((bytes[i] >>> 4).toString(16));
        hex.push((bytes[i] & 0xF).toString(16));
    }
    return hex.join("");
}



module.exports = {
    arrToHex,
    strToBytes,
    asciiToHex,
    hexToBytes,
    bytesToHex
}
