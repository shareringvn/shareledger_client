"use strict"

let  RpcClient  = require('./tendermint/rpc')
//import { RpcClient } from './tendermint';
let defaultUrl = 'ws://localhost:26657'
//let base64 = require('base-64');
let utf8 = require('utf8');

const QUERY_PATH = '"app/query"'

class Client {

    constructor(url) {

        let inputUrl = url || defaultUrl
        this.rpcClient = RpcClient(inputUrl)
        this.closed = false;
        this.rpcClient.on('error', err => {
            console.log("connection:", err);
        });
        //this.rpcClient.unsafeFlushMempool()//set -rpc.unsafe to run this mode   
    }

    close() {
        this.close = true
        this.rpcClient.close()
    }
    status() {
        return this.rpcClient.status();
    }

    chainID() {
        return this.rpcClient.status()
            .then(status => status.node_info.network);
    }

    height() {
        return this.rpcClient.status()
            .then(status => status.sync_info.latest_block_height);
    }

    headers(minHeight, maxHeight) {
        return this.rpcClient.blockchain({
            minHeight,
            maxHeight
        });
    }

    header(height) {
        return this.headers(height, height)
            .then(head => head.block_metas[0]);
    }

    block(height) {

        return this.rpcClient.block({
            height
        })
    }

    async sendTx(tx) {
        if (typeof tx !== 'string') {
            tx = tx.toString('base64');
        }
        //decode tx from base64
       // let bytes = base64.decode(tx);
       // tx = utf8.decode(bytes);  

        let resp = await this.rpcClient.broadcastTxCommit({
            tx
        });
        if (resp.check_tx.code) {
            console.log("Error in CheckTx: ", resp.check_tx);
        }
        if (resp.deliver_tx.code) {
            console.log("Error in DeliverTx:", resp.deliver_tx);
        }

        return {
            hash: resp.hash,
            height: resp.height,
            deliver_tx: resp.deliver_tx
        };
    }



    async txSearch(query, perPage) {
        console.log(query)
        return this.rpcClient.txSearch({
                query,
                perPage
            })
            .catch(err => console.log(err));
    }

    // abciQuery returns  a promise
    abciQuery(data) {
        //path = path || "/";
        if (typeof data !== 'string') {
            data = data.toString('hex');
        }
        //decode tx from base64
        //let bytes = base64.decode(data);
        //data = utf8.decode(bytes);

        return this.rpcClient.abciQuery({
            path: QUERY_PATH,
            data: data,
        });
    }


    async query(data) {
        let q = await this.abciQuery(data)
        return q
    }

    subscribeTx( query, callback) {   
        //todo: add more logic ?     
        this.rpcClient.subscribe({query}, callback);
    }




}

module.exports = Client
