let {
    Codec,
    TypeFactory,
    Types
} = require('js-amino');
const base64 = require('base-64');
let Utils = require("../../utils")


let MsgDelete = TypeFactory.create('MsgDelete', [   
    {
        name: "uuid",
        type: Types.String
    }
])

getSignBytes = msgDelete => {
   /* let tmpObj = JSON.parse(JSON.stringify(msgDelete.JsObject()))
    tmpObj.creator = Utils.asciiToHex(tmpObj.creator)
    tmpObj.hash = base64.encode(tmpObj.hash) //in Blockchain:[]bye->encodeBase64:https://golang.org/pkg/encoding/json/
    let bz = JSON.stringify(tmpObj)    */
    return JSON.stringify(msgDelete.JsObject());
}



registerConcrete = codec => {
    codec.registerConcrete(new MsgDelete(), "shareledger/asset/MsgDelete", {});
}

create = obj => {
    let msgDelete = new MsgDelete(obj.uuid)
    return msgDelete;
}

module.exports = {
    create,
    registerConcrete,
    getSignBytes
}