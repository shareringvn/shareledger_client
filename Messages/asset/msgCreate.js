let {
    Codec,
    TypeFactory,
    Types
} = require('js-amino');
const base64 = require('base-64');
let Utils = require("../../utils")


let MsgCreate = TypeFactory.create('MsgCreate', [{
        name: "creator", //address of asset creator
        type: Types.ByteSlice
    },
    {
        name: "hash",
        type: Types.String
    },
    {
        name: "uuid",
        type: Types.String
    },
    {
        name: "status",
        type: Types.Boolean
    },
    {
        name: "fee",
        type: Types.Int64
    }
])

getSignBytes = msgCreate => {
    let tmpObj = JSON.parse(JSON.stringify(msgCreate.JsObject()))
    // address in signature always in UpperCase
    tmpObj.creator = Utils.bytesToHex(tmpObj.creator).toUpperCase()

    tmpObj.hash = base64.encode(tmpObj.hash) //in Blockchain:[]bye->encodeBase64:https://golang.org/pkg/encoding/json/
    let bz = JSON.stringify(tmpObj)
    return bz;
}



registerConcrete = codec => {
    codec.registerConcrete(new MsgCreate(), "shareledger/asset/MsgCreate", {});
}

create = obj => {
    let msgCreate = new MsgCreate(Utils.hexToBytes(obj.creator),
                                obj.hash,
                                obj.uuid,
                                obj.status,
                                obj.fee)

    return msgCreate;
}

module.exports = {
    create,
    registerConcrete,
    getSignBytes
}
