let {
    Codec,
    TypeFactory,
    Types
} = require('js-amino');
const base64 = require('base-64');
let Utils = require("../../utils")


let MsgRetrieve = TypeFactory.create('MsgRetrieve', [   
    {
        name: "uuid",
        type: Types.String
    }
])

getSignBytes = msgRetrieve => { 
    return JSON.stringify(msgDelete.JsObject());
}



registerConcrete = codec => {
    codec.registerConcrete(new MsgRetrieve(), "shareledger/asset/MsgRetrieve", {});
}

create = uuid => {
    let msgRetrieve = new MsgRetrieve(uuid)
    return msgRetrieve;
}

module.exports = {
    create,
    registerConcrete,
    getSignBytes
}