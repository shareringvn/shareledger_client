let {
    Codec,
    TypeFactory,
    Types
} = require('js-amino');
const base64 = require('base-64');
let Utils = require("../../utils")


let MsgUpdate = TypeFactory.create('MsgUpdate', [{
        name: "creator", //address of asset creator
        type: Types.String
    },
    {
        name: "hash",
        type: Types.String
    },
    {
        name: "uuid",
        type: Types.String
    },
    {
        name: "status",
        type: Types.Boolean
    },
    {
        name: "fee",
        type: Types.Int64
    }
])

getSignBytes = msgUpdate => {
    let tmpObj = JSON.parse(JSON.stringify(msgUpdate.JsObject()))
    // address in signature always in upperCase
    tmpObj.creator = Utils.bytesToHex(tmpObj.creator).toUpperCase()

    tmpObj.hash = base64.encode(tmpObj.hash) //in Blockchain:[]bye->encodeBase64:https://golang.org/pkg/encoding/json/
    let bz = JSON.stringify(tmpObj)
    console.log(bz)    
    return bz;
}



registerConcrete = codec => {
    codec.registerConcrete(new MsgUpdate(), "shareledger/asset/MsgUpdate", {});
}

create = obj => {
    let msgUpdate = new MsgUpdate(obj.creator,obj.hash,
        obj.uuid, obj.status, obj.fee)

    return msgUpdate;
}

module.exports = {
    create,
    registerConcrete,
    getSignBytes
}
