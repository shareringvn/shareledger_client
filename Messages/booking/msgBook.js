let {
    Codec,
    TypeFactory,
    Types
} = require('js-amino');

let Utils = require("../../utils")


let MsgBook = TypeFactory.create('MsgBook', [{
        name: "uuid",
        type: Types.String
    },
    {
        name: "duration",
        type: Types.Int64
    }
])

getSignBytes = msgBook => {
    let tmpObj = JSON.parse(JSON.stringify(msgBook.JsObject()))
    let bz = JSON.stringify(tmpObj)
    return bz;
}



registerConcrete = codec => {
    codec.registerConcrete(new MsgBook(), "shareledger/booking/MsgBook", {});
}

create = obj => {
    let msgBook = new MsgBook(obj.uuid, obj.duration)

    return msgBook;
}

module.exports = {
    create,
    registerConcrete,
    getSignBytes
}
