let {
    Codec,
    TypeFactory,
    Types
} = require('js-amino');

let Utils = require("../../utils")


let MsgComplete = TypeFactory.create('MsgComplete', [{
        name: "bookingId",
        type: Types.String
    }
])

getSignBytes = msgComplete => {
    let tmpObj = JSON.parse(JSON.stringify(msgComplete.JsObject()))
    let bz = JSON.stringify(tmpObj)
    return bz;
}



registerConcrete = codec => {
    codec.registerConcrete(new MsgComplete(), "shareledger/booking/MsgComplete", {});
}

create = obj => {
    let msgComplete = new MsgComplete(obj.bookingId)
    return msgComplete;
}

module.exports = {
    create,
    registerConcrete,
    getSignBytes
}
