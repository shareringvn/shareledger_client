let {
    Codec,
    TypeFactory,
    Types
} = require('js-amino');

let Utils = require("../../utils")


let MsgNonce = TypeFactory.create('MsgNonce', [
    {
        name: "address",
        type: Types.ByteSlice
    }
])

getSignBytes = msgNonce => {
    let tmpObj = JSON.parse(JSON.stringify(msgNonce.JsObject()))

    // UpperCase
    tmpObj.address = Utils.bytesToHex(tmpObj.address).toUpperCase()

    let bz = JSON.stringify(tmpObj)
    return bz;
}



registerConcrete = codec => {
    codec.registerConcrete(new MsgNonce(), "shareledger/auth/MsgNonce", {});
}

create = address => {
    let msgNonce = new MsgNonce(Utils.hexToBytes(address))
    return msgNonce;
}

module.exports = {
    create,
    registerConcrete,
    getSignBytes
}
