let {
    Codec,
    TypeFactory,
    Types
} = require('js-amino');
let Utils = require("../../utils")





let MsgCheck = TypeFactory.create('MsgCheck', [{
    name: "account",
    type: Types.ByteSlice
}])


getSignBytes = msgCheck => {
    let tmpObj = JSON.parse(JSON.stringify(msgCheck.JsObject())) //deep-copy   
    tmpObj.account = Utils.asciiToHex(tmpObj.account)
    let bz = JSON.stringify(tmpObj)
    return bz;
}

registerConcrete = codec => {
    codec.registerConcrete(new MsgCheck(), "shareledger/bank/MsgCheck", {});
}

create = account => {
    let msgCheck = new MsgCheck(Utils.hexToBytes(account))
    return msgCheck;
}

module.exports = {
    create,
    registerConcrete,
    getSignBytes
}
