let {
    Codec,
    TypeFactory,
    Types
} = require('js-amino');

let Utils = require("../../utils")

let Coin = require("../../Types/coin")



let MsgLoad = TypeFactory.create('MsgLoad', [   
    {
        name: "address",
        type: Types.ByteSlice
    },
    {
        name: "amount",
        type: Types.Struct
    }
])


getSignBytes = msgLoad => {
    let tmpObj = JSON.parse(JSON.stringify(msgLoad.JsObject())) //deep-copy
    //tmpObj.address = Utils.asciiToHex(tmpObj.address)
    tmpObj.address = Utils.bytesToHex(tmpObj.address).toUpperCase()
    let bz = JSON.stringify(tmpObj)
    return bz;
}

registerConcrete = codec => {
    codec.registerConcrete(new MsgLoad(), "shareledger/bank/MsgLoad", {});
}

create = creditObj => {
    let coin = Coin.create(creditObj.amount)
    let msgLoad = new MsgLoad( Utils.hexToBytes(creditObj.from),  coin)

    return msgLoad;
}

module.exports = {
    create,
    registerConcrete,
    getSignBytes
}
