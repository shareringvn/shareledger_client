let {
    Codec,
    TypeFactory,
    Types
} = require('js-amino');

let Utils = require("../../utils")

let Coin = require("../../Types/coin")


let MsgSend = TypeFactory.create('MsgSend', [
    {
        name: "to",
        type: Types.ByteSlice
    },
    {
        name: "amount",
        type: Types.Struct
    }
])


getSignBytes = msgSend => {
    let tmpObj = JSON.parse(JSON.stringify(msgSend.JsObject()))
   // tmpObj.from ='31323334353637'//dirty testing ->BC convert from []byte->string ->sign string,JS needs to sign the same string
   // tmpObj.to = '32333435363738'
  //  tmpObj.from = Utils.asciiToHex(tmpObj.from)
    //tmpObj.to = Utils.asciiToHex(tmpObj.to)
    tmpObj.to = Utils.bytesToHex(tmpObj.to).toUpperCase()
    let bz = JSON.stringify(tmpObj)
    return bz;
}



registerConcrete = codec => {
    codec.registerConcrete(new MsgSend(), "shareledger/bank/MsgSend", {});
}

create = sendingObj => {
    let coin = Coin.create(sendingObj.amount)
    let msgSend = new MsgSend(
        /*sendingObj.nonce, sendingObj.from,*/
        Utils.hexToBytes(sendingObj.to),
        coin )
    
        return msgSend;   
}

module.exports = {
    create,
    registerConcrete,
    getSignBytes
}
