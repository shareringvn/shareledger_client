"use strict";
let  Client  = require('./client.js')

let ShareLedger = require('./index.js')
let crypto = require('shr-keys')

//let client = new Client('http://localhost:46657') // local
let client = new Client('http://192.168.1.234:56657') // sharering


let path = '"app/query"'


let testLoadCoin = function () {
    let privateKey = "ab83994cf95abe45b9d8610524b3f8f8fd023d69f79449011cb5320d2ca180c5"

    let acc = crypto.KeyPair.fromPrivateKey(privateKey)

    let nonceQueryTx = ShareLedger.createNonceQueryTx("405C725BC461DCA455B8AA84769E8ACE6B3763F4")

    console.log("Address:", acc.address)

    
    client.query(nonceQueryTx).
        then((result) => {

            console.log("Balance:", result.response.log)
            let nonce = parseInt(result.response.log)

            console.log("Load credit")
            let creditTx = ShareLedger.createCreditTx(acc.address.toUpperCase(), 10, "SHRP", nonce + 1, acc)
            
            console.log("Tx:", creditTx)
            return client.sendTx(creditTx)
        }).
        then((res) => {
            console.log("Result:", res)
        })
}


let testSendCoin = function () {

    let fromPrivKey = "ab83994cf95abe45b9d8610524b3f8f8fd023d69f79449011cb5320d2ca180c5"
    let fromAcc = crypto.KeyPair.fromPrivateKey(fromPrivKey)


    let toPrivKey = "E81785A9994729CCB47D673A815D52E17031F631D6214AF075071B6D0E92A68E"
    let toAcc = crypto.KeyPair.fromPrivateKey(toPrivKey)
    
    console.log("Get Nonce of From Account")
    let nonceQueryTx = ShareLedger.createNonceQueryTx(fromAcc.address.toUpperCase())
    let nonce = 0
    client.query(nonceQueryTx).
        then((result) => {
            console.log("Result:", result)

            console.log("Nonce:", result.response.log)
            nonce = parseInt(result.response.log)

            console.log("Load coins")

            nonce += 1

            let creditTx = ShareLedger.createCreditTx(fromAcc.address.toUpperCase(), 10, "SHRP", nonce, fromAcc)
            return client.sendTx(creditTx)
        }).then((result) => {
            console.log("Result:", result)

            nonce += 1

            console.log("Send SHRP")

            let sendTx = ShareLedger.createSendTx(toAcc.address.toUpperCase(), 1, "SHRP", nonce, fromAcc)
            return client.sendTx(sendTx)
        }).
        then((res) => {
            console.log("Result:", res)
        })

}


let testBooking = function() {
    let fromPrivKey = "ab83994cf95abe45b9d8610524b3f8f8fd023d69f79449011cb5320d2ca180c5"
    let fromAcc = crypto.KeyPair.fromPrivateKey(fromPrivKey)

    let nonceQuery = ShareLedger.createNonceQueryTx(fromAcc.address.toUpperCase())

    let toPrivKey = "E81785A9994729CCB47D673A815D52E17031F631D6214AF075071B6D0E92A68E"
    let toAcc = crypto.KeyPair.fromPrivateKey(toPrivKey)
    
    console.log("Get Nonce of From Account")

    let nonce = 1
    let _balance = 12
    let _fee = 1
    let _duration = 5
    let _uuid = "12"

    client.query(nonceQuery).
        then((result) => {
            nonce = parseInt(result.response.log)
            console.log("Current Nonce: ", nonce)

            nonce += 1
            console.log("Deposit:")
            let creditTx = ShareLedger.createCreditTx(fromAcc.address.toUpperCase(), _balance, "SHR", nonce, fromAcc)
            return client.sendTx(creditTx)
        }).
        then((result) => {
            console.log("Result:", result)
            nonce += 1
            console.log("Create Asset")
            let assetObj = {
                creator: fromAcc.address.toUpperCase(),
                hash: "MzMzMzMz",
                uuid: _uuid,
                status: true,
                fee: _fee,
            }
            let assetCreationTx = ShareLedger.createAssetCreationTx(assetObj, nonce,fromAcc)

            return client.sendTx(assetCreationTx)
        }).
        then((result) => {
            nonce += 1

            console.log("Asset creation result:", result)

            let bookingObj = {
                uuid: _uuid,
                duration: _duration
            }

            let bookingTx = ShareLedger.createBookingTx(bookingObj, nonce, fromAcc)

            return client.sendTx(bookingTx)
        }).
        then((result) => {
            console.log("Result:", result)

            let bookingId = JSON.parse(result.deliver_tx.log).bookingId

            console.log("BookingId: ", bookingId)

            console.log("Complete booking")

            nonce += 1

            let bookingObj = {
                renter: fromAcc.address,
                bookingId: bookingId
            }

            let completeTx = ShareLedger.createBookingCompletionTx(bookingObj, nonce, fromAcc)

            return client.sendTx(completeTx)

        }).
        then((result) => {
            console.log("Result:", result)
        }).
        catch(err => {
            console.log("err=",err)
        })

}

//testLoadCoin()
testSendCoin()
//testBooking()



