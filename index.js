//"use strict";

const base64 = require('base-64');
const utf8 = require('utf8');
const Client = require("./client"); // Mobile platform cannot support
const KeyPair = require('shr-keys').KeyPair



// All Messages used in ShareLedger
const MsgSend = require("./Messages/bank/msgSend")
const MsgLoad = require("./Messages/bank/msgLoad")
const MsgCheck = require("./Messages/bank/msgCheck")
const MsgCreate = require("./Messages/asset/msgCreate")
const MsgDelete = require("./Messages/asset/msgDelete")
const MsgRetrieve = require("./Messages/asset/msgRetrieve")
const MsgUpdate = require("./Messages/asset/msgUpdate")
const MsgNonce = require("./Messages/auth/msgNonce")

const MsgBook = require("./Messages/booking/msgBook")
const MsgComplete = require("./Messages/booking/msgComplete")
const Signature = require("./Types/signature")
const Tx = require("./Types/tx")
const Utils = require("./utils")


let {
    Codec
} = require('js-amino');

let codec = new Codec()

let registerConcrete = cdc => {
    MsgSend.registerConcrete(cdc);
    MsgLoad.registerConcrete(cdc)
    MsgCheck.registerConcrete(cdc)
    MsgCreate.registerConcrete(cdc)
    MsgDelete.registerConcrete(cdc)
    MsgRetrieve.registerConcrete(cdc)
    MsgUpdate.registerConcrete(cdc)
    MsgBook.registerConcrete(cdc)
    MsgComplete.registerConcrete(cdc)
    MsgNonce.registerConcrete(cdc)
    Signature.registerConcrete(cdc)
    Tx.registerConcrete(cdc)

};

registerConcrete(codec); // register types to Amino encoder/decoder

const TxType = {
    SEND: "sender",
    CREDIT: "account",
    CHECK: "check"

}

const BalanceChangedType = {
    IN_ADDRESS: "msg.To", //money come to address
    OUT_ADDRESS: "msg.From", //money go out of address
    CREDIT_ADDRESS: "account" //money was credited to address
}

/**********************************************************************************
 * UTILITIES
 *
 **********************************************************************************/


/*
 * marshalBinary - encode tx in Amnino
 */
let marshalBinary = tx => {
    let binary = codec.marshalBinary(tx)
    let txHex = Utils.arrToHex(binary)
    return txHex
}

/*
 * Remove 0x prefix if presents
 * @param {string} a hex string prefixed with 0x or not
 * @return {string} a hex string without 0x
 */
function cleanHex(str) {
    if (str.startsWith("0x"))
        return str.slice(2)
    else
        return str
}

function encodeWSRequest(request) {
    let strTx = JSON.stringify(request)
    strTx = strTx.replace(/(:\s*"\w+")/g, (match) => {
        return match.replace(/"/g, "'")
    })
    strTx = '"'.concat(strTx).concat('"')
    return strTx
}

function encodeHttpRequest(request) {
    //console.log(JSON.stringify(request, null, 2))
    let txt = JSON.stringify(request)
    let encoded = txt.replace(/"/g, '\\"')
    let enclosed = '"'.concat(encoded).concat('"')
    //encode to base64
    // serialize the object for cross-machine transmission
    let bytes = utf8.encode(enclosed);
    encoded = base64.encode(bytes);

    return encoded;

}



/**********************************************************************************
 * MESSAGES CREATION FUNCTIONS
 *
 **********************************************************************************/

/***** BANKING MESSAGES *****/

/*
 * createCreditTx - load tokens/coins into an account
 * @param {string} address - hex string address of the account - IN UPPERCASE
 * @param {integer} amount - value
 * @param {string} coinName - "SHR" or "SHRP"
 * @param {integer} nonce - number of transaction so far
 * @param {KeyPair} keyPair - key pair generated from mnemonic using shr-keys
 * @return {string} hex representation of binary Amino-encode tx
 */
let createCreditTx = (address, amount, coinName, nonce, keyPair) => {
    let creditObj = {
        nonce: nonce,
        from: address,
        amount: {
            denom: coinName,
            amount: amount
        }
    }
    let creditTx = Tx.createCreditTx(creditObj, keyPair)
    let txHex = marshalBinary(creditTx)
    return txHex;
}


/*
 * createSendTx - send coins from one to another account
 * @param {string} to - hex string address of the account - IN UPPERCASE
 * @param {string} amount - value
 * @param {string} coinName - "SHR" or "SHRP"
 * @param {integer} nonce - number of transaction so far
 * @param {KeyPair} keyPair - key pair generated from mnemonic using shr-keys
 * @return {string} hex representation of binary Amino-encode tx
 */
let createSendTx = (to, amount, coinName, nonce, keyPair) => {
    let sendingObj = {
        nonce: nonce,
        to: to,
        amount: {
            denom: coinName,
            amount: amount
        }
    }
    let sendTx = Tx.createSendTx(sendingObj, keyPair)
    let txHex = marshalBinary(sendTx)
    return txHex;
}


/*
 * make a query of checking balance of certain address
 * @param {string} address - address
 * @return {string} hex representation of binary Amino-encode tx
 */
let createCheckBalanceTx = address => {
    let checkTx = Tx.createCheckBalanceTx(address)
    let txHex = marshalBinary(checkTx)
    return txHex;
}


/***** ASSET MESSAGES ****/

/**
 * make an tx of asset creation
 * @param {Object} assetObj - Object contains properties for new asset:
 * * creator{string}: address of creator of this asset
 * * hash{string}: Hash value of all asset information
 * * uuid{string}: uuid of asset
 * *status{boolean}: True = Available, False = Unavailable. Boolean value indicating whether an asset is rentable
 * *fee(number}: Price per time unit 
 * @return {Object} Response of transaction
 */
let createAssetCreationTx = (assetObj,nonce, keyPair) => {
    assetObj.nonce = nonce
    let tx = Tx.createAssetCreationTx(assetObj, keyPair)
    let txHex = marshalBinary(tx)
    return txHex;
}

/**
 * make an tx of asset creation
 * @param {Object} assetObj - Object contains properties for updating asset:
 * * creator{string}: creator of this asset
 * * hash{string}: Hash value of all asset information
 * * uuid{string}: uuid of asset 
 * @return {Object} Response of transaction
 */
function createAssetUpdateTx(assetObj,nonce, keyPair) {
    assetObj.nonce = nonce
    let tx = Tx.createAssetUpdateTx(assetObj, keyPair)
    let txHex = marshalBinary(tx)
    return txHex;
}

/**
 * make an tx of asset creation
 * @param {Object} assetObj - Object contains properties for delete asset:
 * * uuid{string}: uuid of asset 
 * @return {Object} Response of transaction
 */
function createAssetDeleteTx(uuid, nonce, keyPair) {
    let deleteObj = { uuid:uuid, nonce: nonce }
    let tx = Tx.createAssetDeleteTx(deleteObj, keyPair)
    let txHex = marshalBinary(tx)
    return txHex;
}

/**
 * retrieve an asset
 * @param {string} uuid -  uuid of asstes 
 * @return {string} binary tx
 */

function createAssetRetrievalTx(uuid,keyPair) { 
    let tx = Tx.createAssetRetrievalTx(uuid)
    let txHex = marshalBinary(tx)
    return txHex;
}


// Only for demo
//function listAssets(requestorAddress, client, keyPair) {
    //let queryObj = {
        //type: MsgType.ASSET_LISTING,
        //value: {
            //requestor: requestorAddress
        //}
    //}
    //let query = encodeHttpRequest(queryObj)
    //return client.query(path, query)

//}



/******** BOOKING MESSAGES *********/

/**
 * make an tx of asset booking
 * @param {Object} bookingObj - Object contains properties for booking and asset:
 * * uuid{string}: uuid of asset 
 * * duration(number): length of renting period 
 * @return {Object} Response of transaction with "log" is the booking information
 */
function createBookingTx(bookingObj, nonce, keyPair) {
    bookingObj.nonce = nonce
    let tx = Tx.createBookingTx(bookingObj, keyPair)
    let txHex = marshalBinary(tx)
    return txHex;
}

/**
 * make an tx of  booking completion
 * @param {Object} bookingObj - Object contains properties for booking and asset:
 * * bookingId{string}: uuid of asset  
 * @return {Object} Response of transaction with "log" is the  updated booking information
 */
function createBookingCompletionTx(bookingObj, nonce, keyPair) {
    bookingObj.nonce = nonce
    let tx = Tx.createBookingCompletionTx(bookingObj, keyPair)
    let txHex = marshalBinary(tx)
    return txHex;
}

/*********** AUTH MESSAGES *********************/

/**
 * createNonceQueryTx - retrieve current nonce of an account
 * @param {string} address - hex string address of the account - IN UPPERCASE
 * @return {string} hex representation of binary Amino-encode tx
 */
let createNonceQueryTx = address => {
    let tx = Tx.createNonceQueryTx(address)
    let txHex = marshalBinary(tx)
    return txHex;
}


/************* LISTINGS & EVENT LISTENERS *****************/

function listBankTx(address, type, client) {
    let txQuery = '"' + type + '=\'' + address + '\'"'
    return client.txSearch(txQuery, 100) //return a promise
}

function balanceChanged(address, client, type, callback) {
    let queryEvt = "ShareLedgerEvt" + " = 'BalanceChanged'";
    let queryAddress = type + "='" + address + "'"
    let finalQuery = queryEvt + " AND " + queryAddress
    client.subscribeTx(finalQuery, callback)
}




/********* EXPORTS ************/


module.exports = {
    createCreditTx,
    createSendTx,
    balanceChanged,
    TxType,
    BalanceChangedType,
    createAssetCreationTx,    
    createAssetDeleteTx,
    createAssetUpdateTx,
    //listAssets,
    listBankTx,
    createBookingCompletionTx,
    createBookingTx,
    createCheckBalanceTx,
    createAssetRetrievalTx,
    createNonceQueryTx,
    Client
}


if (require.main == module) {
    let mnemonic = KeyPair.createMnemonic()
    console.log("Mnemonic:", mnemonic)

    let keyPair = KeyPair.fromMnemonic(mnemonic)
    console.log("KeyPair:", keyPair)

    // let tx = createCreditTx("112233", 1, "SHR", 1, keyPair)
    // console.log("TX:", tx)

    // let tx1 = createCreditTx("223344", 1, "SHR", 1 )
    // console.log("TX1:", tx1) 
    let sendTx = createSendTx("123445", "223344", 1, "SHR", 1, keyPair)
    console.log("sendTx:", sendTx)

}





/*
 * Remove 0x prefix if presents
 * @param {string} a hex string prefixed with 0x or not
 * @return {string} a hex string without 0x
 */
function cleanHex(str) {
    if (str.startsWith("0x"))
        return str.slice(2)
    else
        return str
}
