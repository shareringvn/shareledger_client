"use strict";
//let  Client  = require('./client.js')

const KeyPair = require('shr-keys').KeyPair
let ShareLedger = require('./index.js')
let Client = ShareLedger.Client
let client = new Client('http://localhost:46656')
//let clientSocket = new Client('ws://localhost:46657')

let mnemonic = KeyPair.createMnemonic()


let keyPair = KeyPair.fromMnemonic(mnemonic)
let creationObj = {
  creator: "123456",
  hash: "105BC1F548A9173B579E4A56DD836FD73624C777CC368884E654DED81474B141",
  uuid: "123",
  status: true,
  fee: 500
}

let updateObj = {
  creator: "123456",
  hash: "105BC1F548A9173B579E4A56DD836FD73624C777CC368884E654DED81474B141",
  uuid: "123",
  status: false,
  fee: 12345
}

let bookingObj = {
  renter: "23456",  
  uuid: "123",
  duration: 5000,
  
}

let bookingCompletionObj = {
  renter: "23456",  
  bookingId: "1234",
}

let creditTx = ShareLedger.createCreditTx(keyPair.address, 100, "SHR", 1, keyPair)
let sendTx = ShareLedger.createSendTx("2345678", 10, "SHR", 2, keyPair)
let creationTx = ShareLedger.createAssetCreationTx(creationObj, 1, keyPair)
let deleteTx = ShareLedger.createAssetDeleteTx("123",1,keyPair)
let updateTx = ShareLedger.createAssetUpdateTx(updateObj, 1, keyPair)
let bookingTx = ShareLedger.createBookingTx(bookingObj,1,keyPair)
let bookingCompletionTx = ShareLedger.createBookingCompletionTx(bookingCompletionObj,1,keyPair)

let checkTx = ShareLedger.createCheckBalanceTx(keyPair.address)
let retrievalTx = ShareLedger.createAssetRetrievalTx("2345678")
let queryNonceTx = ShareLedger.createNonceQueryTx(keyPair.address)
let path = '"app/query"'





let func = async () => {
  try {

    // ShareLedger.balanceChanged("123456", clientSocket, ShareLedger.BalanceChangedType.OUT_ADDRESS, (data, err) => {
    //  console.log("balance changed:",data) 

    //} )



    /*let resultCredit = await client.sendTx(creditTx)
    console.log(resultCredit)

    let resultSendTx = await client.sendTx(sendTx)
    console.log(resultSendTx)
    let result2 = await client.query(path, checkTx)
    console.log(result2)*/

   // let creationResult = await client.sendTx(bookingCompletionTx)
   // console.log(creationResult)

   let result2 = await client.query(path, queryNonceTx)
    console.log(result2)


    // ShareLedger.listTx("123456",ShareLedger.TxType.SEND,client).then(txs =>{
    //  console.log(txs)
    // })



  } catch (error) {
    console.log(error)
  }
}
func()



//ShareLedger.checkBalance("123456",client)